package org.blockface.virtualshopgui.gui;

import org.bukkit.entity.Player;
import org.getspout.spoutapi.player.SpoutPlayer;

import java.util.HashMap;

public class WindowManager 
{
    private static HashMap<String,ShopWindow> windows = new HashMap<String, ShopWindow>();

    public static void openWindow(Player player) 
    {
        ShopWindow window = getWindow(player);
        window.open();
    }

    public static ShopWindow getWindow(Player player) 
    {
        if(windows.containsKey(player.getName())) return windows.get(player.getName());
        System.out.println("Creating window.");
        ShopWindow window = new ShopWindow((SpoutPlayer)player);
        windows.put(player.getName(),window);
        return window;
    }

    public static void removeWindow(String player) 
    {
        windows.remove(player);
    }
}
