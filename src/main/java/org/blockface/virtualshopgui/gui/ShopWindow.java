package org.blockface.virtualshopgui.gui;

import org.blockface.virtualshop.managers.DatabaseManager;
import org.blockface.virtualshop.objects.Offer;
import org.blockface.virtualshopgui.VirtualShopGUI;
import org.getspout.spoutapi.gui.*;
import org.getspout.spoutapi.player.SpoutPlayer;

import java.util.HashMap;
import java.util.List;

public class ShopWindow extends GenericPopup 
{
    private Container whole;
    private Container items;
    private SpoutPlayer player;
    private OrderBox orderBox;

    private HashMap<Button,Offer> buttons = new HashMap<Button, Offer>();

    public ShopWindow(SpoutPlayer player) 
    {
        super();
        this.setBgVisible(true);
        this.player = player;
        buildScreen();
    }

    public void open() 
    {
        closeOrderBox();
        fillItems();
        player.getMainScreen().attachPopupScreen(this);
    }

    public void closeOrderBox() 
    {
        if(orderBox==null) return;
        orderBox.clearWidgets();
        whole.removeChild(orderBox);
        orderBox=null;
    }

    private void buildScreen() 
    {
        //Setup Whole Window
        whole = new GenericContainer();
        whole
                .setAlign(WidgetAnchor.CENTER_CENTER)
                .setLayout(ContainerType.OVERLAY)
                .setWidth(300)
                .setHeight(200)
                .setAnchor(WidgetAnchor.CENTER_CENTER)
                .shiftXPos(-150)
                .shiftYPos(-100);
                this.attachWidget(VirtualShopGUI.getInstance(), whole);

        //Setup Background
        whole.addChild(
                new GenericGradient()
                        .setTopColor(new Color(0.46F, 0.46F, 0.46F, 1.0F))
                        .setBottomColor(new Color(0.46F, 0.46F, 0.46F, 1.0F))
                        .setPriority(RenderPriority.Highest));

        //Setup Title
        Container title = new GenericContainer();
        title
                .setAlign(WidgetAnchor.CENTER_CENTER)
                .setWidth(300);
        whole.addChild(title);
        Label label = new GenericLabel();
        label
                .setText("MarketPlace")
                .setResize(true)
                .setFixed(true)
                .setMargin(2);
        title.addChild(label);
        
        //Setup Offers
        items = new GenericContainer();
        whole.addChild(items);
        items
                .setLayout(ContainerType.OVERLAY)
                .setWidth(300)
                .setHeight(180)
                .setMarginTop(20);

        this.setDirty(true);
    }

    public void fillItems() 
    {
        removeItems();
        List<Offer> offers = DatabaseManager.GetBestPrices();
        int x=10;
        int y=0;
        for(Offer o : offers) {
            Button button = new GenericButton();
            button
                    .setHeight(20)
                    .setWidth(20)
                    .setMarginTop(y)
                    .setMarginLeft(x)
                    .setFixed(true)
                    .setTooltip(o.item.getAmount() + " for " + o.price + " each");
            items.addChild(button);
            buttons.put(button,o);
            ItemWidget iw = new GenericItemWidget(o.item);
            iw
                    .setWidth(8)
                    .setHeight(8)
                    .setDepth(8)
                    .setFixed(true)
                    .setMarginLeft(x+2)
                    .setMarginTop(y+2)
                    .setPriority(RenderPriority.Low);
            items.addChild(iw);
            x+=20;
            if(x==290) 
            {
                x=10;
                y+=20;
            }
        }
    }

    public SpoutPlayer getPlayer() 
    {
        return this.player;
    }

    public void onClick(Button button) 
    {
        if(orderBox!=null) 
        {
            if(orderBox.onClick(button)) return;
        }
        closeOrderBox();
        Offer o = buttons.get(button);
        orderBox = new OrderBox(o,this);
        whole.addChild(orderBox);
    }
    
    private void removeItems() 
    {
        buttons = new HashMap<Button, Offer>();
        for(Widget w: items.getChildren()) 
        {
            items.removeChild(w);
        }
    }
}
