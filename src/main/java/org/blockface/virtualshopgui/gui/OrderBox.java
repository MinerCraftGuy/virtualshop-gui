package org.blockface.virtualshopgui.gui;

import org.blockface.virtualshop.objects.Offer;
import org.blockface.virtualshop.util.ItemDb;
import org.getspout.spoutapi.gui.*;
import org.getspout.spoutapi.player.SpoutPlayer;

public class OrderBox extends GenericContainer 
{
    private Offer offer;
    private Button buy;
    private GenericTextField quantity;
    private ShopWindow parent;

    public OrderBox(Offer offer, ShopWindow parent) 
    {
        this.parent = parent;
        this.offer = offer;
        this
                .setLayout(ContainerType.OVERLAY)
                .setPriority(RenderPriority.Lowest)
                .setHeight(70)
                .setWidth(100)
                .setMarginTop(65)
                .setMarginLeft(100)
                .setAnchor(WidgetAnchor.CENTER_CENTER);
        buildMenu();
    }

    public boolean onClick(Button button) 
    {
        if(button!=buy) return false;
        String amount = quantity.getText();
        String command = "/buy " + amount + " " + ItemDb.reverseLookup(offer.item);
        parent.getPlayer().chat(command);
        parent.fillItems();
        parent.closeOrderBox();
        return true;
    }

    public void clearWidgets() 
    {
        for(Widget w: this.getChildren()) 
        {
            this.removeChild(w);
        }
    }

    private void buildMenu() 
    {
        final Color color = new Color(0.2F,0.2F,0.2F,1.0F);
        this.addChild(
                new GenericGradient()
                        .setTopColor(color)
                        .setBottomColor(color)
                        .setWidth(100)
                        .setHeight(70)
                        .setFixed(true)
                        .setPriority(RenderPriority.Low));
        this.addChild(
                new GenericLabel("Quantity:")
                        .setFixed(true)
                        .setMarginTop(5)
                        .setMarginLeft(5)
                        .setPriority(RenderPriority.Lowest));
        quantity = new GenericTextField();
        this.addChild(
                quantity
                        .setText(offer.item.getAmount() + "")
                        .setFixed(true)
                        .setMarginTop(20)
                        .setMarginLeft(5)
                        .setHeight(18)
                        .setWidth(90)
                        .setPriority(RenderPriority.Lowest));

        buy = new GenericButton("Buy");
        this.addChild(
                buy
                        .setAnchor(WidgetAnchor.TOP_LEFT)
                        .setMarginTop(45)
                        .setMarginLeft(5)
                        .setFixed(true)
                        .setWidth(90)
                        .setHeight(20)
                        .setPriority(RenderPriority.Lowest));
    }
}