package org.blockface.virtualshopgui.events;

import org.blockface.virtualshopgui.gui.ShopWindow;
import org.blockface.virtualshopgui.gui.WindowManager;
import org.getspout.spoutapi.event.screen.ButtonClickEvent;
import org.getspout.spoutapi.event.screen.ScreenListener;

public class ScreenEvents extends ScreenListener 
{
    @Override
    public void onButtonClick(ButtonClickEvent event) 
    {
        ShopWindow window = WindowManager.getWindow(event.getPlayer());
        window.onClick(event.getButton());
    }
}
