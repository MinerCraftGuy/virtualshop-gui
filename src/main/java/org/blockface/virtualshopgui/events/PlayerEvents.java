package org.blockface.virtualshopgui.events;

import org.blockface.virtualshopgui.gui.WindowManager;
import org.bukkit.event.player.PlayerListener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerEvents extends PlayerListener
{
    @Override
    public void onPlayerQuit(PlayerQuitEvent event) 
    {
        WindowManager.removeWindow(event.getPlayer().getName());
    }
}

