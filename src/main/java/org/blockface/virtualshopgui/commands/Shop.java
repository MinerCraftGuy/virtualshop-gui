package org.blockface.virtualshopgui.commands;

import org.blockface.virtualshopgui.gui.WindowManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Shop implements CommandExecutor
{
    public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) 
    {
        if(!(sender instanceof Player)) return false;
        Player player = (Player)sender;
        WindowManager.openWindow(player);
        return true;
    }
}
