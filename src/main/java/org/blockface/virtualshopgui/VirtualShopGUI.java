package org.blockface.virtualshopgui;

import org.blockface.virtualshopgui.bukkitstats.CallHome;
import org.blockface.virtualshopgui.commands.Shop;
import org.blockface.virtualshopgui.events.KeyBoardListener;
import org.blockface.virtualshopgui.events.PlayerEvents;
import org.blockface.virtualshopgui.events.ScreenEvents;
import org.bukkit.event.Event;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class VirtualShopGUI extends JavaPlugin 
{

    private static Plugin instance;

    public void onDisable() 
    {
        System.out.println(this + " is now disabled!");
    }

    public void onEnable() 
    {
        instance = this;
        CallHome.load(this);
        getCommand("shop").setExecutor(new Shop());
        PluginManager pm = this.getServer().getPluginManager();
        pm.registerEvent(Event.Type.CUSTOM_EVENT, new ScreenEvents(), Event.Priority.Normal, this);
        pm.registerEvent(Event.Type.PLAYER_QUIT, new PlayerEvents(), Event.Priority.Normal, this);
        pm.registerEvent(Event.Type.CUSTOM_EVENT, new KeyBoardListener(), Event.Priority.Normal, this);
        System.out.println(this + " is now enabled!");
    }

    public static Plugin getInstance() 
    {
        return instance;
    }
}